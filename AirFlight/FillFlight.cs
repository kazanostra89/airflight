﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirFlight
{
    class FillFlight
    {
        private List<Flight> flights;

        /*
        public FillFlight()
        {
            flights = new List<Flight>();
            Play();
        }
        */

        public FillFlight()
        {
            flights = new List<Flight>()
            {
                new Flight(0, 1, Convert.ToDateTime("25.05.2020 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 3),
                new Flight(0, 2, Convert.ToDateTime("26.05.2020 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 2),
                new Flight(1, 0, Convert.ToDateTime("27.05.2020 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 2),
                new Flight(1, 3, Convert.ToDateTime("27.05.2020 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 0),
                new Flight(2, 1, Convert.ToDateTime("28.05.2020 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 1),
                new Flight(2, 3, Convert.ToDateTime("29.05.2021 14:50"), Convert.ToDateTime("26.05.2020 02:00"), 0)
            };
        }

        private Flight AddToListFlight()
        {
            Console.WriteLine("Начало заполнения записи!");

            Flight flight = new Flight
                (
                    InputCheckAirport("Выберите код: Апатиты(0), Брянск(1), Мурманск(2), Пулково(3), Череповец(4)-> "),
                    InputCheckAirport("Выберите код: Апатиты(0), Брянск(1), Мурманск(2), Пулково(3), Череповец(4)-> "),
                    InputCheckDateTime("Введите дату и время вылета-> "),
                    InputCheckDateTime("Введите дату и время прилета-> "),
                    InputCheckStatus("Выберите код: Вылетел(0), Прилетел(1), Задерживается(2), Производится посадка(3)-> ")
                );

            Console.WriteLine("Запись добавленна, нажмите любую клавишу для продолжения!");

            return flight;
        }

        private void PrintAllFlight()
        {
            Console.WriteLine("{0,-14}{1,-14}{2,-16}{3,-16}{4,-20}", "Аэр. вылета", "Аэр. прилета", "Дата вылета", "Дата прилета", "Статус");

            if (flights.Count == 0)
            {
                Console.WriteLine("Расписание не заполено!\n");
            }
            else
            {
                foreach (Flight item in flights)
                {
                    item.PrinFlight();
                }
            }
        }

        private void PrintMenuFillingFlight()
        {
            Console.WriteLine("1. Добавить запись");
            Console.WriteLine("2. Очистить список");
            Console.WriteLine("3. Поиск по аэропорту вылета/прилета");
            Console.WriteLine("4. Поиск по дате вылета/прилета");
            Console.WriteLine("5. Поиск статусу");
            Console.WriteLine("0. Выйти\n");
        }

        public void Play()
        {
            bool fillingFlight = true;

            while (fillingFlight)
            {
                Console.Clear();
                PrintAllFlight();
                PrintMenuFillingFlight();

                int selectMenu = InputCheckInt("Выберете соответствующий пункт меню: ");

                switch (selectMenu)
                {
                    case 1:
                        flights.Add(AddToListFlight());
                        break;
                    case 2:
                        flights.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Апатиты(0), Брянск(1), Мурманск(2), Пулково(3), Череповец(4)");
                        int airNumber = InputCheckAirport("Для фильтрации введите соответствующий числовой код: ");

                        bool airSelector = InputBool("Тип фильтрации аэропорта Вылет(true) Прилет(false): ");

                        Console.Clear();
                        FilterAirport(airNumber, airSelector);
                        break;
                    case 4:
                        DateTime date = InputCheckDateTime("Введите дату по формату (дд.мм.гггг): ");

                        bool dateSelector = InputBool("Тип фильтрации даты Вылет(true) Прилет(false): ");

                        Console.Clear();
                        FilterDate(date, dateSelector);
                        break;
                    case 5:
                        Console.WriteLine("Вылетел(0), Прилетел(1), Задерживается(2), Производится посадка(3)");
                        int status = InputCheckStatus("Для фильтрации введите соответствующий числовой код: ");

                        Console.Clear();
                        FilterStatus(status);
                        break;
                    case 0:
                        fillingFlight = false;
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда");
                        break;
                }

                Console.ReadKey();
            }

        }

        #region Функции для фильтрации данных в коллекции

        public void FilterAirport(int x, bool selector)
        {
            Console.WriteLine(new string('*', 70));

            if (flights.Count == 0)
            {
                Console.WriteLine("Расписание не заполнено!");
            }
            else if (selector)
            {
                foreach (Flight item in flights)
                {
                    if (item.AirportDeparture == (Flight.Airport)x)
                    {
                        item.PrinFlight();
                    }
                }
            }
            else
            {
                foreach (Flight item in flights)
                {
                    if (item.AirportArrival == (Flight.Airport)x)
                    {
                        item.PrinFlight();
                    }
                }
            }

            Console.WriteLine(new string('*', 70));
        }

        public void FilterDate(DateTime x, bool selector)
        {
            Console.WriteLine(new string('*', 70));

            if (flights.Count == 0)
            {
                Console.WriteLine("Расписание не заполнено!");
            }
            else if (selector)
            {
                foreach (Flight item in flights)
                {
                    if (item.DepartureDateTime.ToShortDateString() == x.ToShortDateString())
                    {
                        item.PrinFlight();
                    }
                }
            }
            else
            {
                foreach (Flight item in flights)
                {
                    if (item.ArrivalDateTime.ToShortDateString() == x.ToShortDateString())
                    {
                        item.PrinFlight();
                    }
                }
            }

            Console.WriteLine(new string('*', 70));
        }

        public void FilterStatus(int x)
        {
            if (flights.Count == 0)
            {
                Console.WriteLine("Расписание не заполнено!");
            }
            else
            {
                foreach (Flight item in flights)
                {
                    if (item.Status == (Flight.StatusFlight)x)
                    {
                        item.PrinFlight();
                    }
                }
            }
        }

        #endregion

        #region Функции для проверки вводимых значений

        private int InputCheckInt(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        private bool InputBool(string message)
        {
            bool check;
            bool log;

            do
            {
                Console.Write(message);
                check = bool.TryParse(Console.ReadLine(), out log);

            } while (check == false);

            return log;
        }

        private DateTime InputCheckDateTime(string message)
        {
            bool check;
            DateTime date;

            do
            {
                Console.Write(message);
                check = DateTime.TryParse(Console.ReadLine(), out date);

            } while (check == false);

            return date;
        }

        private int InputCheckStatus(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < 0 || number > 3);

            return number;
        }

        private int InputCheckAirport(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < 0 || number > 4);

            return number;
        }

        #endregion
    }
}
