﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirFlight
{
    class Flight
    {
        #region Перечисления

        public enum Airport
        {
            Апатиты = 0,
            Брянск = 1,
            Мурманск = 2,
            Пулково = 3,
            Череповец = 4
        }
        
        public enum StatusFlight
        {
            Вылетел = 0,
            Прилетел = 1,
            Задерживается = 2,
            ПроизводитсяПосадка = 3
        }
        
        #endregion

        private Airport airDeparture;
        private Airport airArrival;
        private DateTime departureDateTime;
        private DateTime arrivalDateTime;
        private StatusFlight status;

        public Flight(int airDeparture, int airArrival, DateTime departureDateTime, DateTime arrivalDateTime, int status)
        {
            this.airDeparture = (Airport)airDeparture;
            this.airArrival = (Airport)airArrival;
            this.departureDateTime = departureDateTime;
            this.arrivalDateTime = arrivalDateTime;
            this.status = (StatusFlight)status;
        }

        public void PrinFlight()
        {
            Console.WriteLine("{0,-14}{1,-14}{2,-16}{3,-16}{4,-20}", airDeparture, airArrival, departureDateTime.ToString("dd.MM.yy hh:mm"), arrivalDateTime.ToString("dd.MM.yy hh:mm"), status);
        }

        #region Свойства, необходимые для фильтрации списка коллекции

        public Airport AirportDeparture { get { return airDeparture; } }

        public Airport AirportArrival { get { return airArrival; } }

        public DateTime DepartureDateTime { get { return departureDateTime; } }

        public DateTime ArrivalDateTime { get { return arrivalDateTime; } }

        public StatusFlight Status { get { return status; } }

        #endregion

    }
}
